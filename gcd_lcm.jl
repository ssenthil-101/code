function gcd(a, b)
    min = a < b ? a : b
    max = a > b ? a : b
    reminder = max % min

    while reminder != 0
        max = min
        min = reminder
        reminder = max % min
    end

    min
end

function lcm(a, b)
    a * b / gcd(a, b)
end
